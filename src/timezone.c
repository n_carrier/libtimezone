#define _DEFAULT_SOURCE
#include <errno.h>
#include <stdbool.h>

#include <dirent.h>
#include <unistd.h>

#include <argz.h>

#include "timezone.h"

#define TIMEZONE_PATH "/etc/localtime"

static int not_dir_filter(const struct dirent *d)
{
	return d->d_type != DT_DIR;
}

static void free_str(char **str)
{
	if (str == NULL)
		return;

	free(*str);
	*str = NULL;
}

int tz_get_sub_timezones(const char *timezone, char **sub_timezones,
		size_t *length)
{
	struct dirent **namelist;
	int n;
	int ret;

	if (sub_timezones == NULL || length == NULL)
		return -EINVAL;

	/* creates an empty argz */
	ret = argz_create_sep("", '\0', sub_timezones, length);
	if (ret != 0)
		return -ret;

	n = scandir(timezone, &namelist, not_dir_filter, alphasort);
	if (n == -1)
		return errno == ENOTDIR ? 0 : -errno;
	while (n--) {
		argz_add(sub_timezones, length, namelist[n]->d_name);
		free(namelist[n]);
	}
	free(namelist);

	return 0;
}

const char *tz_next_timezone(const char *timezone, const char *sub_timezones,
		size_t length)
{
	if (sub_timezones == NULL)
		return NULL;

	return argz_next(sub_timezones, length, timezone);
}

char *tz_get_current(void)
{
	return realpath(TIMEZONE_PATH, NULL);
}

int tz_set(const char *timezone)
{
	char __attribute__((cleanup(free_str)))*canon_tz = NULL;
	int ret;

	if (timezone == NULL)
		return -EINVAL;

	canon_tz = realpath(timezone, NULL);
	if (canon_tz == NULL)
		return -errno;

	ret = unlink(TIMEZONE_PATH);
	if (ret == -1)
		return -errno;
	ret = symlink(canon_tz, TIMEZONE_PATH);
	if (ret == -1)
		return -errno;

	return 0;
}
