# libtimezone

Simple implementation of a timezone manipulation library.

## Build

```
make
```

## Execute

```
sudo LD_LIBRARY_PATH=. ./tz_example
```