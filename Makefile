CFLAGS = -Wall -Wextra -O0 -g -I include

all:libtimezone.so tz_example

tz_example:libtimezone.so example/main.c
	$(CC) $^ -o $@ $(CFLAGS)

libtimezone.so: src/timezone.c
	$(CC) $^ -shared -fPIC -o $@ $(CFLAGS)

clean:
	rm libtimezone.so tz_example

.PHONY: clean