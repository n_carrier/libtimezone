#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>

#include "timezone.h"

static void free_str(char **str)
{
	if (str == NULL)
		return;

	free(*str);
	*str = NULL;
}

int main(void)
{
	char __attribute__((cleanup(free_str)))*timezone = NULL;
	char __attribute__((cleanup(free_str)))*sub_timezones = NULL;
	size_t length;
	int ret;
	const char *cur;
	const char *upper_timezone = "/usr/share/zoneinfo/Asia/";

	timezone = tz_get_current();
	printf("Current time zone is %s\n", timezone);

	ret = tz_set("/usr/share/zoneinfo/Asia/Jakarta");
	if (ret < 0)
		perror("tz_set: %m");

	/* during this sleep, one can see that the time is now offset */
	sleep(3);

	ret = tz_set(timezone);
	if (ret < 0)
		perror("tz_set: %m");

	ret = tz_get_sub_timezones(upper_timezone, &sub_timezones, &length);
	if (ret < 0) {
		sub_timezones = NULL;
		fprintf(stderr, "tz_get_sub_timezones: %s\n", strerror(-ret));
		return EXIT_FAILURE;
	}

	cur = NULL;
	while ((cur = tz_next_timezone(cur, sub_timezones, length)) != NULL)
		printf("%s%s\n", upper_timezone, cur);

	return EXIT_SUCCESS;
}
