#ifndef INCLUDE_TIMEZONE_H_
#define INCLUDE_TIMEZONE_H_
#include <stdlib.h>

/* sub_timezones must be freed after usage */
int tz_get_sub_timezones(const char *timezone, char **sub_timezones,
		size_t *length);
/* iterate over sub timezones, pass NULL as timezone to get the first element */
const char *tz_next_timezone(const char *timezone, const char *sub_timezones,
		size_t length);
/* returned string must be freed after usage */
char *tz_get_current(void);
int tz_set(const char *timezone);

#endif /* INCLUDE_TIMEZONE_H_ */
